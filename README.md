# mcp3002

A spade driver for the mcp3002 ADC

## Usage

Add this library as a submodule to your project

```toml
[libraries]
mcp3002 = {git = "https://gitlab.com/TheZoq2/mcp3002-spade", branch="main"}
```

